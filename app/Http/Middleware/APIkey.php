<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\User;

class APIkey
{
    protected $except = [
        '/',
        'docs',
        'api/login',
        'api/register',
        'api/documentation',
    ];
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!in_array($request->path(), $this->except))
        {
            if ($request->api_access == '') 
            {
                return redirect('/');
            }else{ 
                $users = User::where('api_access', $request->api_access)->count();
                if ($users != 1) 
                { 
                    return response('Unauthorized.', 401);        
                } else {
                    $response = $next($request);
                    return $response;
                }
            }
    
        } else{
            $response = $next($request);
            return $response;
        }
    }
}
