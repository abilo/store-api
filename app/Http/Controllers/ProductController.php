<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/products?api_access={api_access}",
     *      operationId="index",
     *      tags={"Products"},
     *      summary="Get List Of Products",
     *     @OA\Parameter(
     *        name="api_access", in="path",required=true, @OA\Schema(type="string")
     *     ),
     *      security= {{"bearerAuth":{}}},   
     *      description="Returns all products.",
     * @OA\Response(
     *      response=200,
     *      description="Successful operation",
     *      @OA\MediaType(
     *      mediaType="application/json",
     *   )
     * ),
     * @OA\Response(
     *      response=401,
     *      description="Unauthenticated",
     * ),
     * @OA\Response(
     *      response=403,
     *      description="Forbidden"
     * ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *  )
     */
    public function index()
    {        
        try{
            return Product::with('variants.images')->get();
        }catch(\Exception $e){
            return response()->json([
                'status'=>'error',
                'message'=>$e->getMessage()
            ]);    
        }

    }

    /**
     * @OA\Get(
     *      path="/api/products/{id}?api_access={api_access}",
     *      @OA\Parameter(
     *         name="id",in="path",required=true,@OA\Schema(type="integer"),
     *     ),
     *     @OA\Parameter(
     *        name="api_access", in="path",required=true, @OA\Schema(type="string")
     *     ),
     *      tags={"Product"},
     *      summary="Get Detail Of one Product",
     *      security= {{"bearerAuth":{}}},   
     *      description="Returns one product.",
     * @OA\Response(
     *      response=200,
     *      description="Successful operation",
     *      @OA\MediaType(
     *      mediaType="application/json",
     *   )
     * ),
     * @OA\Response(
     *      response=401,
     *      description="Unauthenticated",
     * ),
     * @OA\Response(
     *      response=403,
     *      description="Forbidden"
     * ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *  )
     */    
    public function get_product(Request $request, $id)
    {        
        try{
            $product = Product::find($id);
            if($product) return Product::with('variants.images')->where('id',$id)->get();
            else return response('Not found.', 404);        

        }catch(\Exception $e){
            return response()->json([
                'status'=>'error',
                'message'=>$e->getMessage()
            ]);    
        }

    }

}
