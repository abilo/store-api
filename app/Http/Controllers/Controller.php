<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *   title="eGora API",
     *   version="1.0",
     *   description="This is an API that scrape and sync data from retailer and members eGora online and serve the data in JSON format",
     *   @OA\Contact(
     *     email="saadi.nabil@gmail.com",
     *     name="Software engineer"
     *   )
     * )
     */
}
