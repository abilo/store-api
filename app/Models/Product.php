<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @package Petstore30
 *
 * @author  Nabil SAADI <author@email.com>
 *
 * @OA\Schema(
 *     description="Product model",
 *     title="Product model",
 *     required={"id", "title","body_html","handle","product_type"},
 *     @OA\Xml(
 *         name="Product"
 *     )
 * )
 */

class Product extends Model
{
    use HasFactory;
    /**
     * @OA\Property(property="id",type="auto")
     * @OA\Property(property="title",type="string")
     * @OA\Property(property="body_html",type="string")
     * @OA\Property(property="handle",type="string")
     * @OA\Property(property="product_type",type="integer")
     *
     * @return array
     */
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */    
    protected $guarded=[];

// Relationship

    public function variants()
    {
        return $this->hasMany(Variant::class, 'product_id','id');
    }

}
