<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Stock
 *
 * @package Petstore30
 *
 * @author  Nabil SAADI <author@email.com>
 *
 * @OA\Schema(
 *     description="Stock model",
 *     title="Stock model",
 *     required={"id","qte","variant_id"},
 *     @OA\Xml(
 *         name="Stock"
 *     )
 * )
 */
class Stock extends Model
{
    use HasFactory;
    /**
     * @OA\Property(property="id",type="auto")
     * @OA\Property(property="qte",type="integer")
     * @OA\Property(property="in_qte",type="integer")
     * @OA\Property(property="out_qte",type="integer")
     * @OA\Property(property="adjust_qte",type="integer")
     * @OA\Property(property="current",type="boolean")
     * @OA\Property(property="variant_id",type="integer")
     *
     * @return array
     */
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */    
    protected $guarded=[];

// Relationship

    public function variants()
    {
        return $this->belongsTo(Variant::class, 'variant_id','id');
    }

}
