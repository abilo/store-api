<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Variant
 *
 * @package Petstore30
 *
 * @author  Nabil SAADI <saadi.nabil@email.com>
 *
 * @OA\Schema(
 *     description="Variant model",
 *     title="Variant model",
 *     required={"sku","price","taxable","weight","requires_shipping","size","status","price_promotion","product_id"},
 *     @OA\Xml(
 *         name="Variant"
 *     )
 * )
 */
class Variant extends Model
{
    use HasFactory;
    /**
     * @OA\Property(property="id",type="auto")
     * @OA\Property(property="sku",type="string")
     * @OA\Property(property="price",type="double")
     * @OA\Property(property="taxable",type="boolean")
     * @OA\Property(property="weight",type="string")
     * @OA\Property(property="requires_shipping",type="boolean")
     * @OA\Property(property="size",type="enum")
     * @OA\Property(property="status",type="enum")
     * @OA\Property(property="price_promotion",type="double")
     * @OA\Property(property="product_id",type="integer")
     *
     * @return array
     */
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */    
    protected $guarded=[];

// Relationship

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imagetable');
    }

}
