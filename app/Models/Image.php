<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 *
 * @package Petstore30
 *
 * @author  Nabil SAADI <saadi.nabil@email.com>
 *
 * @OA\Schema(
 *     description="Image model",
 *     title="Image model",
 *     required={"imagetable_id","imagetable_type"},
 *     @OA\Xml(
 *         name="Image"
 *     )
 * )
 */
class Image extends Model
{
    use HasFactory;
    /**
     * @OA\Property(property="id",type="auto")
     * @OA\Property(property="imagetable_id",type="integer")
     * @OA\Property(property="imagetable_type",type="string")
     * @OA\Property(property="size",type="string")
     * @OA\Property(property="format",type="string")
     *
     * @return array
     */
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */    
    protected $fillable = [
        'imagetable_id',
        'imagetable_type',
        'name',
        'size',
        'format'
    ];

// Relationship
    public function imagetable()
    {
        return $this->morphTo();
    }

}
