# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://img.shields.io/packagist/dt/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://img.shields.io/packagist/v/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://img.shields.io/packagist/l/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Download and configuration

- Download the Store API on your machine.
- Create a database on your machine with the name db_egora as mentioned in the .env file or change the name as you wish in the .env file (DB_DATABASE=db_store).
- Open the command line and go to the root of the project folder.
- To launch Store-Api type this command: php -S localhost:8080 -t public
- To check that your Api is working properly, type in your browser http://localhost:8080.
- Then type this command: "php artisan migrate" to migrate and create tables in your database.
- Type this command: "php artisan db:seed" to populate the database with fake data to help you test with data.


## Swagger as an annotation and documentation

- To start working and testing Store-Api, you need to use Postman or use Swagger (Swagger is an interface description language for describing RESTful APIs expressed using JSON).
We can use Swagger as an annotation and documentation of any API.

## Test API by Swagger

- Now to start using Swagger, type this link http://localhost:8080/api/documentation into your browser.
- Before testing the Swagger documentation for your API, open your database to see what data you have in your database.
- If you want to test the API without an Access API, for example to get the product list, you will get an "Unauthorised" error and even it will ask you for an "api_access". In this case, you need to have an Api access. So click on the Register operation to create an account and get an Api Access.
- After creating your account, you will receive an Api Access, which can help you to use it and perform other operations such as get the list of products and view the details of any product.
- To perform the GET Products operation, you need to fill in the api_access field with the Api you got after registration, and then click the Execute button to view the result. The same principle applies to the display of any product, but you need the product ID that you have in your database.

## Security

Note: I create my own API using a middleware here is my middleware ...\store-apiapp\Http\Middleware\APIkey without using JWT or Passwport, because the API will work between two services or two machines, in this case we don't need to provide the username and password to get a token.... etc. 

For example the API of google maps or any other service on the internet always offers you an API after registration or subscription. This is just a simple and efficient API, we can work further and add more parameters to make our API more secure.
       
For retailers who already have Magento, we will use Store-API "without the security part" and work directly with the Magento API to get the token delivered by the magento API.

## Model and Logic

To see the model of Store-API, go to ..store-api and choose a model to see the model and the relationship between the models.
To see the logic of Store-API, go to ..\store-apiapp\Http\Controllers and click on roductController.php.
