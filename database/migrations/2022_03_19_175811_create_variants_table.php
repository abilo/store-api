<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variants', function (Blueprint $table) {
            $table->id();
            $table->string('sku');
            $table->double('price');
            $table->boolean('taxable')->default(0);
            $table->string('weight');
            $table->boolean('requires_shipping')->default(0);
            $table->enum('size', ['s','m','l','xl','xxl'])->default('s');
            $table->enum('status', ['active','disable','cancel'])->default('active');
            $table->double('price_promotion');

            $table->bigInteger('product_id');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variants');
    }
}
