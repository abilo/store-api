<?php

namespace Database\Factories;

use App\Models\Variant;
use Illuminate\Database\Eloquent\Factories\Factory;

class VariantFactory extends Factory
{
    protected $model = Variant::class;

    public function definition(): array
    {
        return [
            'sku' => strtoupper($this->faker->bothify('?###??###??')),
            'price' =>$this->faker->randomFloat(2, 15, 250),
            'taxable' => $this->faker->boolean,
            'weight' =>$this->faker->randomElement(['250 g','1 kg','73 g','120 g']),
            'requires_shipping' =>$this->faker->boolean,
            'size' =>$this->faker->randomElement(['s','m','l','xl','xxl']),
            'status' =>$this->faker->randomElement(['active','disable','cancel']),
            'price_promotion' =>$this->faker->randomFloat(2, 15, 250),
            'product_id' =>$this->faker->randomNumber(1,10)
        ];
    }
}
