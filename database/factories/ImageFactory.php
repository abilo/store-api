<?php

namespace Database\Factories;

use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Factory;

class ImageFactory extends Factory
{
    protected $model = Image::class;

    public function definition(): array
    {
    	return [
            'imagetable_id' =>$this->faker->randomNumber(1,10),
            'imagetable_type' => 'App\Models\Variant',
            'name' => $this->faker->imageUrl(400, 240),
            'size' => $this->faker->randomNumber(1,150),
            'format' =>$this->faker->randomElement(['jpg','png','gif','jpeg','svg']),
    	];
    }
}
